#version 450 core

uniform float iGlobalTime;
uniform vec2 iResolution;

layout (location = 0) out vec4 color;

mat2 sinCosMatrix(float a)
{
    float c=cos(a);
    float s=sin(a);
    return mat2(c,-s,s,c);
}

float map(vec3 p)
{
    p.xz*= sinCosMatrix(iGlobalTime*0.4);
    p.xy*= sinCosMatrix(iGlobalTime*0.3);

    vec3 q = p * 2.0 + iGlobalTime;
    float length_value = length(p+vec3(sin(iGlobalTime*0.7)));
    float log_value = log(length(p)+1.0);
    return length_value * log_value + sin(q.x+sin(q.z+sin(q.y)))*0.5 - 1.;
}

void main(){    
    vec2 p = gl_FragCoord.xy/iResolution.y - vec2(1.0,0.5);
    vec3 cl = vec3(0.);
    float d = 2.5;
    for(int i=0; i<=5; i++) {
        vec3 p = vec3(0,0,5.) + normalize(vec3(p, -1.))*d;
        float rz = map(p);
        float f =  clamp((rz - map(p+.1))*0.5, -.1, 1. );
        vec3 l = vec3(0.1,0.3,.4) + vec3(5., 2.5, 3.)*f;
        cl = cl*l + (1.-smoothstep(0., 2.5, rz))*.7*l;
        d += min(rz, 1.);
    }
    color = vec4(cl, 1.);
}