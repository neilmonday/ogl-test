#version 430 core
#extension GL_ARB_enhanced_layouts : require

layout(points)                   in;
layout(points, max_vertices = 1) out;

layout (xfb_buffer = 0, xfb_offset = 0)  out     vec4 gs_fs;
layout (xfb_buffer = 1, xfb_stride = 64) out;

layout (binding    = 0)                  uniform gs_block {
    vec4 uni_gs;
};

void main()
{
    gs_fs  = uni_gs;
    gl_Position  = vec4(0, 0, 0, 1);
    EmitVertex();
}