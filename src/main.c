#include <stdio.h>
#include <assert.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "program.h"

void window_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void main(void)
{
    GLFWwindow* window;
    // Initialise GLFW
    if (!glfwInit())
        return;

    int initialWidth = 1280;
    int initialHeight = 720;

    // Create a fullscreen mode window and its OpenGL context
    //window = glfwCreateWindow(initialWidth, initialHeight, "OGL-TEST", glfwGetPrimaryMonitor(), NULL);
    // Create a windowed mode window and its OpenGL context 
    window = glfwCreateWindow(initialWidth, initialHeight, "OGL-TEST", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return;
    }

    glfwSetWindowSizeCallback(window, &window_size_callback);

    // Make the window's context current
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (GLEW_OK != err)
        return;

    // Initialize 
    GLuint graphics_program = 0;
    build_graphics_program(&graphics_program);

    //create a VAO
    GLuint vertex_array_object;
    glGenVertexArrays(1, &vertex_array_object);
    glBindVertexArray(vertex_array_object);

    GLfloat vertices[] = {
        -1.0f, -1.0f,
         1.0f, -1.0f,
        -1.0f,  1.0f,
         1.0f, -1.0f,
         1.0f,  1.0f,
        -1.0f,  1.0f
    };

	int width = 0, height = 0, previous_width = 0, previous_height = 0;
	glfwGetWindowSize(window, &width, &height);
	err = glGetError(); assert(!err);

    //create a VBO
    GLuint vertex_buffer_object;
    glGenBuffers(1, &vertex_buffer_object);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * 4, vertices, GL_STATIC_DRAW);
    err = glGetError(); assert(!err);

    //inputs
    GLint position_location = glGetAttribLocation(graphics_program, "position");
    glVertexAttribPointer(position_location, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(position_location);
    err = glGetError(); assert(!err);

    // Get a handle to fragment shader's iGlobalTime uniform.
    GLuint globalTimeHandle = glGetUniformLocation(graphics_program, "iGlobalTime");
    GLuint resolutionHandle = glGetUniformLocation(graphics_program, "iResolution");

    GLfloat time = 0.0f;

    // Loop until the user closes the window 
    while (!glfwWindowShouldClose(window))
    {
        glUniform1f(globalTimeHandle, time);
        glUniform2f(resolutionHandle, width, height);

        const GLfloat color[] = { 0.5f, 0.5f, 0.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, color);

		//Draw the image to the screen
		glUseProgram(graphics_program);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		
		glfwSwapBuffers(window);
		glfwPollEvents();
        time += 0.001;
	}

    glDeleteVertexArrays(1, &vertex_array_object);
    glfwTerminate();
    return;
}
